import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestApiService } from '../shared/rest-api.service';
import {  User } from '../shared/order';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user = new User();
  msg = '';
  constructor(
    private service : RestApiService,
    private router: Router) { }

  ngOnInit(): void {
  }

  login(){
    //  console.log(this.register);
      this.service.loginUser(this.user).subscribe(
        data => {
          console.log(data);
          console.log(data.status);
          if(data[0]!=null){
            this.router.navigate(['/dashboard']);
          }
          else
          {
            console.log(data);
            console.log("exception occured");
            this.msg ="Bad credentials, please enter valid email id and password!"
          }
        },
        error => {
          console.log("exception occured");
          this.msg ="Bad credentials, please enter valid email id and password!"
        }
      );   
    }
}
