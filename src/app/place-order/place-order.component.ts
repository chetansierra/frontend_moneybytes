import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ExternalApi_GET, Order } from '../shared/order';
import { RestApiService } from '../shared/rest-api.service';
import { map } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
//import { Chart } from 'chart.js';

@Component({
  selector: 'app-place-order',
  templateUrl: './place-order.component.html',
  styleUrls: ['./place-order.component.css']
})
export class PlaceOrderComponent implements OnInit {
  extapi_get = new ExternalApi_GET();
  search: string = "";
  symbol="";
  msg = '';
  alldata = [];
  price_max = [];
  price_min = [];
  alldates = [];
  weatherDates : string[] = [];
  chart : string[] = [];
  orderDetails = new Order();
  constructor(
    private restApi: RestApiService, 
    private router: Router,
    private route: ActivatedRoute
  ) { 
    
  }

  ngOnInit(): void {
    
    this.route.queryParams
      .subscribe(params => {
        this.symbol = params.symbol;
        console.log(this.symbol); // price
        this.getEnquiryData(this.symbol);
      }
    );
    this.graphMethod();
  }

    submit(){
      this.restApi.placeBuyOrder(this.orderDetails).subscribe(
        data => {
          this.router.navigate(['/dashboard']);
          this.msg="Order Placed";
          console.log("response recieved")
        },
        error => {
          console.log("exception occured");
          this.msg = error.error;
        }
      );   
    }

    getEnquiryData(symbol:string){
      this.restApi.getEnquiryDetails(symbol).subscribe(
        data1=>{
          console.log(data1.data[0]);
          this.orderDetails.stockTicker = symbol;
          this.orderDetails.price = data1.data[5].close;
          this.alldata = data1.data; 
          console.log(this.alldata);
          for (var val of this.alldata) {
            this.price_max.push(val['high']);
            this.price_min.push(val['low']);
            this.alldates.push(val['date']);
          }

          this.weatherDates = []
          this.alldates.forEach((res) => {
            let jsdate = new Date(res * 1000)
            this.weatherDates.push(jsdate.toLocaleTimeString('en', { year: 'numeric', month: 'short', day: 'numeric' }))
          })
          
          console.log(this.price_max);
          console.log(this.price_min);
          console.log(this.alldates);
          console.log(this.weatherDates);
        },
        error => {
          console.log("exception occured");
        }
      )
    
    }
    
    
    graphMethod(){


    }
    
}


