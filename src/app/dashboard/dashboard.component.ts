import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestApiService } from '../shared/rest-api.service';
import { ExternalApi_GET } from '../shared/order';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  extapi_get = new ExternalApi_GET();
  search: string = "";
  symbol="";
  msg = '';
  constructor(private service : RestApiService, private router: Router, ) { }

  ngOnInit(): void {
  }


  getEnquiryData(){
    this.service.getEnquiryDetails(this.extapi_get.symbol).subscribe(
      data=>{
        console.log(data.data);
      },
      error => {
        console.log("exception occured");
      }
    )
  
  }

}
