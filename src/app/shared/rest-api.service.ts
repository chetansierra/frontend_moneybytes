import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../shared/order';
import { Holding, UserDetail, Order } from '../shared/order';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { HoldingsComponent } from '../holdings/holdings.component'; 


@Injectable({
  providedIn: 'root'
})
export class RestApiService {

  apiURL = 'http://localhost:8080/api/trade/';
  apiURLUser = 'http://localhost:8080/api/users/';
  apiURLTrade = 'http://localhost:8080/api/trade/';
  constructor(private http: HttpClient) { }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }  

  
  // HttpClient API get() method 
  getUsersByUsername(name:any): Observable<UserDetail> {
    return this.http.get<UserDetail>(this.apiURLUser + "user" + name)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  } 
  
    // HttpClient API get() method 
    getAllTrades(): Observable<Holding> {
      return this.http.get<Holding>(this.apiURL + "all")
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
    }

    getAllTrades_order(): Observable<Holding> {
      return this.http.get<Holding>(this.apiURL + "all")
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
    }

    getEnquiryDetails(data:string): Observable<any> {
      return this.http.get<any>('http://localhost:8080/api/trade/enquiry?symbol='+data)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
    }
    
    loginUser(user: User):Observable<any>{
      return this.http.post<any>('http://localhost:8080/api/users/login',user);
    }

    registerUser(user: User):Observable<any>{
      return this.http.post<any>(this.apiURLUser,user);
    }

    placeBuyOrder(orderDetails:Order):Observable<any> {
      return this.http.post<any>('http://localhost:8080/api/trade/buy',orderDetails);
    }

     // Error handling 
  handleError(error:any) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
 }
}
