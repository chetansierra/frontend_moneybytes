export class Holding {
    constructor(){
        this.volume = 0;
        this.buyOrSell = "";
        this.price = 0;
        this.time=0;
        this.stockTicker="";
        this.userID=0;
        this.tradeID=0;
        this.statusCode="";
    }
    tradeID:number;
    volume:number;
    price:any;
    statusCode:string;
    time:any;
    stockTicker:string;
    userID:number;
    buyOrSell:string;

}

export class UserDetail {
    constructor(){
        this.id = 0;
        this.fname = "";
        this.lname = "";
        this.email = "";
        this.password = "";
    }
    id:number;
    fname:string;
    lname:string;
    email:string;
    password:string;
    
}

export class ExternalApi {
    constructor(){
        this.pagination;
        this.open;
    }
    pagination:any;
    open:any;
}

export class ExternalApi_GET {
    constructor(){
        this.symbol="";
    }
    symbol:string;
}

export class Order {
    constructor(){
        this.volume = 0;
        this.buyOrSell = "";
        this.price = 0;
        this.time=0;
        this.stockTicker="";
        this.userID=0;
        this.tradeID=0;
        this.statusCode="";
    }
    tradeID:number;
    volume:number;
    price:any;
    statusCode:string;
    time:any;
    stockTicker:string;
    userID:number;
    buyOrSell:string;

}
export class User {
    constructor(){
      this.id=0,
      this.fname="",
      this.lname="",
      this.password="",
      this.email="",
      this.username=""
    }
    
    id: number;
    fname : string;
    lname: string;
    password: string;
    email: string;
    username: string;
   
}


