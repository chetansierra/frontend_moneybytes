import { Component, OnInit } from '@angular/core';
import { RestApiService } from '../shared/rest-api.service';

@Component({
  selector: 'app-order-history',
  templateUrl: './order-history.component.html',
  styleUrls: ['./order-history.component.css']
})
export class OrderHistoryComponent implements OnInit {

  
  Orders: any = [];
  search: string = "";
  constructor( 
    public restApi: RestApiService
    ) { }


  ngOnInit(): void {
    this.loadOrderHistory()
  }

  loadOrderHistory() {
    return this.restApi.getAllTrades().subscribe((data: {}) => {
        this.Orders = data;
        this.formatTime(this.Orders);
    })
  }

  Search(){
    if(this.search!="")
    {
      this.Orders = this.Orders.filter((res: any)=>
      {
        return res.stockTicker.toLocaleLowerCase().match(this.search.toLocaleLowerCase());
      })
    }
    else{
      this.ngOnInit();
    }
   
  };
  formatTime(unix_timestamp : any){
    for(let i=0;i<unix_timestamp.length;i++)
    {
      console.log(unix_timestamp[i].time);
      var date = new Date(unix_timestamp[i].time * 1000);
      // Hours part from the timestamp
      var hours = date.getHours();
      // Minutes part from the timestamp
      var minutes = "0" + date.getMinutes();
      // Seconds part from the timestamp
      var seconds = "0" + date.getSeconds();
      
      // Will display time in 10:30:23 format
      var formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
      this.Orders[i].time = formattedTime;
      console.log(formattedTime);
    }
   }

}
