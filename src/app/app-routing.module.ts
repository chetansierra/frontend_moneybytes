import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HoldingsComponent } from './holdings/holdings.component';
import { OrderHistoryComponent } from './order-history/order-history.component';
import { PlaceOrderComponent } from './place-order/place-order.component';
import { DashboardComponent } from './dashboard/dashboard.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'login' },
  { path: 'register', component: RegisterComponent },
  { path: 'login', component: LoginComponent },
  { path: 'holdings', component: HoldingsComponent },
  { path: 'order-history', component: OrderHistoryComponent },
  { path: 'place-order', component: PlaceOrderComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'place-order/:symbol', component: DashboardComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
