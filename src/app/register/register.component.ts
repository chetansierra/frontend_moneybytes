import { Component, OnInit } from '@angular/core';
import { Router, UrlSegment } from '@angular/router';
import {  User } from '../shared/order';
import { RestApiService } from '../shared/rest-api.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  user = new User();
  msg='';
 constructor(private service : RestApiService, private router: Router) { }
  ngOnInit(): void {
  }

  submit(){
  //  console.log(this.register);
    this.service.registerUser(this.user).subscribe(
      data => {
        this.router.navigate(['/login']);
        this.msg="Registration successful";
        console.log("response recieved")
      },
      error => {
        console.log("exception occured");
        this.msg = error.error;
      }
    );   
  }
}
