import { Component, OnInit } from '@angular/core';
import { RestApiService } from "../shared/rest-api.service";
import { ExternalApi_GET, Order,Holding } from '../shared/order';

@Component({
  selector: 'app-holdings',
  templateUrl: './holdings.component.html',
  styleUrls: ['./holdings.component.css']
})
export class HoldingsComponent implements OnInit {
  search: string = "";
  Holdings: any = [];
  newtime :any;
  newholding = new Holding();
  constructor( 
    public restApi: RestApiService
    ) { }


  ngOnInit(): void {
    this.loadholdings();
   
  }

  loadholdings() {
    return this.restApi.getAllTrades().subscribe((data: {}) => {
        this.Holdings = data;
        this.formatTime(this.Holdings);
      // console.log(this.Holdings[0].time);
    })
  }

  Search(){
    if(this.search!="")
    {
      this.Holdings = this.Holdings.filter((res: any)=>
      {
        return res.stockTicker.toLocaleLowerCase().match(this.search.toLocaleLowerCase());
      })
    }
    else{
      this.ngOnInit();
    }
   
  };

 //let unix_timestamp = 1549312452

formatTime(unix_timestamp : any){
  for(let i=0;i<unix_timestamp.length;i++)
  {
    console.log(unix_timestamp[i].time);
    var date = new Date(unix_timestamp[i].time * 1000);
    // Hours part from the timestamp
    var hours = date.getHours();
    // Minutes part from the timestamp
    var minutes = "0" + date.getMinutes();
    // Seconds part from the timestamp
    var seconds = "0" + date.getSeconds();
    
    // Will display time in 10:30:23 format
    var formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
    this.newtime = formattedTime;
    this.Holdings[i].time = formattedTime;
    console.log(formattedTime);
  }
 }

}